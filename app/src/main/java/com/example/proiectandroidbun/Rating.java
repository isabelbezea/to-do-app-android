package com.example.proiectandroidbun;

import java.io.Serializable;

public class Rating implements Serializable {

    private String id;
    private float rating;
    private String advice;

    public Rating (){}

    public Rating(String id, float rating, String advice) {
        this.id=id;
        this.rating = rating;
        this.advice = advice;
    }

    public Rating(float rating, String advice) {
        this.rating = rating;
        this.advice = advice;
    }

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "rating=" + rating +
                ", advice='" + advice + '\'' +
                '}';
    }
}
