package com.example.proiectandroidbun;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class FreeDaysAdaptor extends BaseAdapter {

    private ArrayList<FreeDay> freeDays;
    private Context context;
    private LayoutInflater inflater;

    public FreeDaysAdaptor(ArrayList<FreeDay> freeDays, Context context) {
        this.freeDays = freeDays;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return freeDays.size();
    }

    @Override
    public Object getItem(int position) {
        return freeDays.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        final View freedayView=inflater.inflate(R.layout.free_day_item, viewGroup, false);
        TextView tvTip=freedayView.findViewById(R.id.tv_tip);
        TextView tvAn=freedayView.findViewById(R.id.tv_an);
        TextView tvDenumire=freedayView.findViewById(R.id.tv_denumire);
        TextView tvData=freedayView.findViewById(R.id.tv_data);
        TextView tvDurata=freedayView.findViewById(R.id.tv_numarzile);
        FreeDay freeDay=freeDays.get(position);
        tvTip.setText(freeDay.getTip());
        tvAn.setText(freeDay.getAn());
        tvDenumire.setText(freeDay.getDenumire());
        tvDurata.setText(freeDay.getDurata());
        tvData.setText(freeDay.getData());
        return freedayView;
    }

    public void addElements(ArrayList<FreeDay> list)
    {
        freeDays.addAll(list);
        notifyDataSetChanged();
    }

    public void  removeElement(int position)
    {
        freeDays.remove(position);
        notifyDataSetChanged();
    }
}
