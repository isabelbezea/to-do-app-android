package com.example.proiectandroidbun;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.proiectandroidbun.database.model.Task;
import com.example.proiectandroidbun.database.service.TaskService;
import com.example.proiectandroidbun.firebase.Callback;
import com.example.proiectandroidbun.firebase.FirebaseService;

import java.util.ArrayList;
import java.util.List;

public class RatingsListActivity extends AppCompatActivity {

    FirebaseService firebaseService;
    private ListView lvRatings;
    public RatingAdapter ratingAdapter;
    public static ArrayList<Rating> rating_list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings_list);

        lvRatings=findViewById(R.id.lv_ratings);
        addAdapter();

        firebaseService=FirebaseService.getInstance();
        firebaseService.attachDataChangeEventListener(dataChangeEventCallback());

    }
    private Callback <List<Rating>> dataChangeEventCallback() {
        return new Callback<List<Rating>>() {
            @Override
            public void runResultOnUiThread(List<Rating> result) {
                if (result != null) {
                    rating_list.clear();
                    rating_list.addAll(result);
                    notifyAdapter();
                }
            }

        };

    }


    private void notifyAdapter() {
        RatingAdapter adapter = (RatingAdapter) lvRatings.getAdapter();
        adapter.notifyDataSetChanged();
    }

    private void addAdapter() {
        ratingAdapter = new RatingAdapter(rating_list, this);
        lvRatings.setAdapter(ratingAdapter);
    }


}