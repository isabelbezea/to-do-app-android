package com.example.proiectandroidbun;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {

    private Button btnCalendar;

   private CustomAdapterSpinner customAdapterSpinner;
   private Button btnFreeDays;
   private Button btnShowTasks;
   private Button btnShowBills;

    public static final String PROFILE_SHARED_PREF="profileSharedPref";
    public static final String USER = "user";

    SharedPreferences prf;
    Intent intent;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnShowBills=findViewById(R.id.button_facturi);
        btnShowBills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MenuActivity.this, BillsActivity.class);
                startActivity(intent);
            }
        });

        btnShowTasks=findViewById(R.id.button_taskuri);
        btnShowTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MenuActivity.this, ToDoTasks.class);
                startActivity(intent);
            }
        });


        btnCalendar=findViewById(R.id.button_calendar);
        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MenuActivity.this, CalendarActivity.class);
                startActivity(intent);
            }
        });

        btnFreeDays=findViewById(R.id.button_sarbatori);
        btnFreeDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MenuActivity.this, FreeDaysActivity.class);
                startActivity(intent);
            }
        });

        displayMessage();

        prf = getSharedPreferences(PROFILE_SHARED_PREF,MODE_PRIVATE);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();

        if(id==R.id.menu_review)
        {
            Intent intent=new Intent(MenuActivity.this, RateActivity.class);
            startActivity(intent);
        }

        if(id==R.id.menu_log_out)
        {
            SharedPreferences.Editor editor = prf.edit();
            editor.clear();
            editor.commit();


            Intent intent=new Intent(MenuActivity.this, MainActivity.class);
            startActivity(intent);

        }

        if(id==R.id.menu_list_reviews)
        {
            Intent intent=new Intent(MenuActivity.this, RatingsListActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


    //populare TextView cu inf din SharedPref
    private void displayMessage(){
        //INSTANTA CATRE FISIERUL DE PREFERINTA
        SharedPreferences preferences=getSharedPreferences(PROFILE_SHARED_PREF , MODE_PRIVATE);
        //PRELUARE NUME DIN FISIER
        String name=preferences.getString(USER, "");
        if(name!= null && !name.isEmpty()){
            String mesaj="Nice to see you, "+name;
            Toast.makeText(this, mesaj, Toast.LENGTH_LONG).show();

        }


        }

}