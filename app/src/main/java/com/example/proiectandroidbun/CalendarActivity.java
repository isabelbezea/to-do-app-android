package com.example.proiectandroidbun;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.CalendarView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class CalendarActivity extends AppCompatActivity {

    private static final String TAG ="CalendarActivity";
    private CalendarView mcalendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

                mcalendarView=(CalendarView) findViewById(R.id.calendarView);
                mcalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int i, int i1, int i2) {
                        //month day year
                        String date = (i1 + 1) + "/" + i2 + "/" + i;
                        Log.v(TAG, "onSelectedDayChange: mm/dd/yyyy" + date);

                        Intent intent = new Intent(CalendarActivity.this,TaskActivity.class);
                        intent.putExtra("date",date);
                        startActivity(intent);
                    }
                });
            }
        }
