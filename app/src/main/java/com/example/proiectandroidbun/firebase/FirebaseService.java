package com.example.proiectandroidbun.firebase;


import android.util.Log;

import androidx.annotation.NonNull;

import com.example.proiectandroidbun.Rating;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseService {

    private static final String RATING_TABLE="ratings";
    private static FirebaseService firebaseService;
    private DatabaseReference database;

    private FirebaseService(){
        database= FirebaseDatabase.getInstance().getReference(RATING_TABLE);
    }

    public static FirebaseService getInstance(){
        if(firebaseService==null) {
            synchronized (FirebaseService.class) {
                if (firebaseService == null) {
                    firebaseService = new FirebaseService();
                }
            }
        }
        return firebaseService;

    }

    public void  upsert(Rating rating){
        if(rating==null) return;

        if(rating.getId()==null || rating.getId().trim().isEmpty()){
        String id=database.push().getKey();
        rating.setId(id);}

        database.child(rating.getId()).setValue(rating);


    }

    public void delete(Rating rating){
        if(rating==null || rating.getId()==null||rating.getId().trim().isEmpty()) return;

        database.child(rating.getId()).removeValue();
        database.child(rating.getId()).removeEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.v("FirebaseService", "Remove is working");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Remove is working");
            }
        });
        }

        public void attachDataChangeEventListener(final Callback<List<Rating>>  callback){
        //apelul metodelor upsert si delete de mai sus forteaza primirea unei notificari de la Firebase in acest eveniment


            //evenimentul este atasat la nivel de coaches (reference) prin urmare asculta orice modificare
            // de insert/update/delete executata asupra acestui nod

            database.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    List<Rating> ratings=new ArrayList<>();
                    for(DataSnapshot data: snapshot.getChildren()){
                        Rating rating=data.getValue(Rating.class);
                        if(rating!=null)
                        {
                            ratings.add(rating);
                        }
                    }
                    callback.runResultOnUiThread(ratings);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.v("FirebaseService", "Data is not available");
                }
            });
        }
    }


