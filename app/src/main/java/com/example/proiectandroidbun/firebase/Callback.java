package com.example.proiectandroidbun.firebase;

public interface Callback<R> {

    void runResultOnUiThread(R result);


}
