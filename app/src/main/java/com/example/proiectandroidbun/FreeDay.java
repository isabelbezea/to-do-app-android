package com.example.proiectandroidbun;

import java.io.Serializable;

public class FreeDay implements Serializable {
    private String denumire;
    private String data;
    private String durata;
    private String an;
    private String tip;

    public FreeDay(){}

    public FreeDay(String denumire, String data, String durata, String an, String tip) {
        this.denumire = denumire;
        this.data = data;
        this.durata = durata;
        this.an = an;
        this.tip = tip;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDurata() {
        return durata;
    }

    public void setDurata(String durata) {
        this.durata = durata;
    }

    public String getAn() {
        return an;
    }

    public void setAn(String an) {
        this.an = an;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    @Override
    public String toString() {
        return "FreeDay{" +
                "denumire='" + denumire + '\'' +
                ", data='" + data + '\'' +
                ", durata='" + durata + '\'' +
                ", an='" + an + '\'' +
                ", tip='" + tip + '\'' +
                '}';
    }
}
