package com.example.proiectandroidbun;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class FreeDaysActivity extends AppCompatActivity {

    public FreeDaysAdaptor freeDaysAdaptor;
    private ListView lvFreeDays;
    public static ArrayList<FreeDay> days_list= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_days);

        lvFreeDays=findViewById(R.id.lv_freedays);
        DownloadFreeManager.getInstance().getFreeDaysData(new IFree() {
            @Override
            public void onSuccess(final ArrayList<FreeDay> result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (days_list.size() == 0)
                            days_list.addAll(result);
                        freeDaysAdaptor=new FreeDaysAdaptor(days_list, FreeDaysActivity.this);
                        lvFreeDays.setAdapter(freeDaysAdaptor);

                        for(FreeDay freeDay: result)
                            Log.v("Remote", freeDay.toString());
                    }
                });
            }

            @Override
            public void onFailure(final Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(FreeDaysActivity.this, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

    }
}