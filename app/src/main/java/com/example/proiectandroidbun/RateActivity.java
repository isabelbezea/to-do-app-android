package com.example.proiectandroidbun;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.proiectandroidbun.database.model.Task;
import com.example.proiectandroidbun.firebase.FirebaseService;

import java.util.ArrayList;
import java.util.List;

public class RateActivity extends AppCompatActivity {

    private RatingBar ratingBar;
    private EditText etReview;
    private Button btnSubmit;
    private int selectedRatingIndex=-1;
    public static float VALOAREA;
    private FirebaseService firebaseService;
    public static ArrayList<Rating> rating_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);

        initViews();
        firebaseService = FirebaseService.getInstance();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    VALOAREA=rating;
                    //Log.v("pentrunoi", String.valueOf(VALOAREA));
            }
        });

    }


    public void initViews()
    {

        ratingBar=findViewById(R.id.ratingBar);
        etReview=findViewById(R.id.et_review);
        btnSubmit=findViewById(R.id.button_submitR);
        btnSubmit.setOnClickListener(saveEventListener());

    }

    private View.OnClickListener saveEventListener(){
        return new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Rating rating=createFromViews();
                firebaseService.upsert(rating);
                Log.v("pentrunoi", "am scris in firebase");
                finish();
            }
        };
    }

    private Rating createFromViews() {


        String review= etReview.getText().toString();
        String id =selectedRatingIndex>=0?rating_list.get(selectedRatingIndex).getId():null;
        float ratingf=ratingBar.getRating();
        Log.v("pentrunoi", String.valueOf(ratingf));
        Log.v("pentrunoi", review);
            return new  Rating(id, ratingf, review);




    }


}