package com.example.proiectandroidbun;

public class Flag {

    private String flag;
    private int flagImage;

    public Flag(String flag, int flagImage) {
        this.flag = flag;
        this.flagImage = flagImage;
    }

    public String getFlag() {
        return flag;
    }

    public int getFlagImage() {
        return flagImage;
    }
}
