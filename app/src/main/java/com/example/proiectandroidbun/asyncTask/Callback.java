package com.example.proiectandroidbun.asyncTask;

public interface Callback<R> {

    void runResultOnUiThread(R result);
}

