package com.example.proiectandroidbun;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.mbms.StreamingServiceInfo;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.proiectandroidbun.asyncTask.Callback;
import com.example.proiectandroidbun.database.model.Account;
import com.example.proiectandroidbun.database.model.Task;
import com.example.proiectandroidbun.database.service.TaskService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TaskActivity extends AppCompatActivity {

    private Button btnTimeStart;
    private Button btnTimeEnd;
    private EditText titlu;
    private EditText descriere;
    private TextView textViewTimeStart;
    private  TextView textViewTimeEnd;
    private Button btnSave;
    private Button btnCancel;
    int chour,cminute;
    private TextView theDate;
    private RadioGroup rgCategory;
    private String tpTimeStart;
    private String tpTimeEnd;
    private String cdate;
    public static final String TASK_KEY = "taskKey";
    private TaskService taskService;
    public static ArrayList<Task> task_list = new ArrayList<>();
    private Intent intent;
    private Task task=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);


        Intent incomingIntent =getIntent();
         cdate =incomingIntent.getStringExtra("date");
        //set the date to the textview
        theDate=findViewById(R.id.tv_cdate);
        theDate.setText("On date: "+cdate);

        initViews();
          //for time picker
                btnTimeStart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //show current time
                        final Calendar calendar=Calendar.getInstance();
                        chour=calendar.get(Calendar.HOUR_OF_DAY);
                        cminute=calendar.get(Calendar.MINUTE);
                        //lounch TimePicker DIALOG
                        TimePickerDialog timePickerDialog=new TimePickerDialog(TaskActivity.this, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                //for time textview show
                                textViewTimeStart.setText(hourOfDay+":"+minute);
                            }
                        },chour,cminute,false);
                        timePickerDialog.show();
                    }
                });

                btnTimeEnd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //show current time
                        final Calendar calendar=Calendar.getInstance();
                        chour=calendar.get(Calendar.HOUR_OF_DAY);
                        cminute=calendar.get(Calendar.MINUTE);
                        //lounch TimePicker DIALOG
                        TimePickerDialog timePickerDialog=new TimePickerDialog(TaskActivity.this, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                //for time textview show
                                textViewTimeEnd.setText(hourOfDay+":"+minute);
                            }
                        },chour,cminute,false);
                        timePickerDialog.show();
                    }
                });

                btnCancel=findViewById(R.id.btnCancel);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });

        intent = getIntent();
        if (intent.hasExtra(TASK_KEY)) {
            task = (Task) intent.getSerializableExtra(TASK_KEY);
            buildViewsFromTask(task);
        }


            }

    private void buildViewsFromTask(Task task) {
        if (task == null) {
            return;
        }
        if (task.getTitle() != null) {
            titlu.setText(String.valueOf(task.getTitle()));
        }
        if (task.getDescription() != null) {
            descriere.setText(String.valueOf(task.getDescription()));
        }
        if (task.getStartTime() != null) {
            textViewTimeStart.setText(String.valueOf(task.getStartTime()));
        }
        if (task.getEndTime() != null) {
            textViewTimeEnd.setText(String.valueOf(task.getEndTime()));
        }
        if(task.getTheDate()!=null){
            theDate.setText("On date: "+ String.valueOf(task.getTheDate()));
        }

        int selectedId= rgCategory.getCheckedRadioButtonId();
        if(selectedId!=-1)
        {
          rgCategory.check(selectedId);
        }

    }


            public void initViews()
            {
                btnTimeStart=(Button)findViewById(R.id.btnStartTime);
                btnTimeEnd=(Button)findViewById(R.id.btnEndTime);

                textViewTimeStart=(TextView)findViewById(R.id.tv_startTime);
                textViewTimeEnd=(TextView)findViewById(R.id.tv_timeEnd);

                titlu=findViewById(R.id.ed_task);
                descriere=findViewById(R.id.ed_descriere);


                rgCategory=findViewById(R.id.rg_forma);

                btnSave=findViewById(R.id.btnSave);

                btnSave.setOnClickListener(saveTaskEventListener());

            }

    private boolean validateContent(){
        if(titlu.getText().toString().isEmpty()){
            Toast.makeText(this, "Title of task must be entered", Toast.LENGTH_LONG).show();
            return false;
        }
        if(descriere.getText().toString().isEmpty()){
            Toast.makeText(this, "Description of task must be entered", Toast.LENGTH_LONG).show();
            return false;
        }

        if(textViewTimeStart.getText().toString().isEmpty())
        {  Toast.makeText(this, "StartTime must be entered", Toast.LENGTH_LONG).show();
            return false;}

        if(textViewTimeEnd.getText().toString().isEmpty())
        {  Toast.makeText(this, "EndTime must be entered", Toast.LENGTH_LONG).show();
            return false;}

        if(rgCategory.getCheckedRadioButtonId() == -1)
        {
            Toast.makeText(this, "A category must be selected", Toast.LENGTH_LONG).show();
            return  false;
        }
        return true;

    }



    private View.OnClickListener saveTaskEventListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateContent()) {
                    createFromViews();
                   intent.putExtra(TASK_KEY, task);
                   //Log.v("pentru noiTaskActivity", task.toString());
                    setResult(RESULT_OK, intent);
                    //Log.v("pentru noiTaskActivity", "trimis");
                    taskService = new TaskService(getApplicationContext());
                    taskService.insert(insertIntoDbCallback(), task);
                    finish();
                }
            }
        };
    }

    private void createFromViews() {

        String titleV=titlu.getText().toString();
        String descriptionV=descriere.getText().toString();
        String starttimeV=textViewTimeStart.getText().toString();
        String endtimeV=textViewTimeEnd.getText().toString();
        int selectedId=rgCategory.getCheckedRadioButtonId();
        RadioButton rbSelected =findViewById(selectedId);
        String categoryV=rbSelected.getText().toString();
        String dateV=cdate;

        if (task == null) {
            task = new Task(titleV, descriptionV, starttimeV, endtimeV, categoryV, dateV);
        } else {
            task.setTitle(titleV);
            task.setDescription(descriptionV);
            task.setCategory(categoryV);
            task.setStartTime(starttimeV);
            task.setEndTime(endtimeV);
            task.setTheDate(dateV);


        }
    }

    private Callback<Task> insertIntoDbCallback() {
        return new Callback<Task>() {
            @Override
            public void runResultOnUiThread(Task result) {
                if (result != null) {
                    task_list.add(result);
                }
            }
        };
    }


        }

