package com.example.proiectandroidbun;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.proiectandroidbun.database.DatabaseManager;
import com.example.proiectandroidbun.database.dao.AccountDao;
import com.example.proiectandroidbun.database.model.Account;

public class MainActivity extends AppCompatActivity {


    public static final String USER = "user";
    public static final String PASSWORD = "password";
    private Button btnLogin;
    private Button btnSignin;
    private EditText user;
    private EditText passwd;
    public static final String PROFILE_SHARED_PREF="profileSharedPref";
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();


        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, SigninActivity.class);
                startActivity(intent);

            }
        });

    }
    private void initComponents(){
        user=findViewById(R.id.et_username);
        passwd=findViewById(R.id.et_password);

        //construire fisier de preferinte
        preferences=getSharedPreferences(PROFILE_SHARED_PREF , MODE_PRIVATE);
        btnSignin=findViewById(R.id.button_signin);
        btnLogin=findViewById(R.id.button_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username=user.getText().toString();
                final String password=passwd.getText().toString();

                //salvarea in fisierul de preferinte
                SharedPreferences.Editor editor=preferences.edit();
                editor.putString(USER,username);
                editor.putString(PASSWORD,password);
                editor.apply();//lipsa duce la pierderea informatiilor scrise in fisier

                DatabaseManager databaseManager=DatabaseManager.getInstance(getApplicationContext());
                final AccountDao accountDao=databaseManager.getAccountDao();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Account account=accountDao.login(username, password);

                        if(account==null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                  final Toast toast =Toast.makeText(MainActivity.this, "User not registered!", Toast.LENGTH_SHORT);
                                  toast.show();
                                }
                            });
                        }else{
                            startActivity(new Intent(MainActivity.this, MenuActivity.class));
                        }
                    }
                }).start();

            }
        });

        loadFromSharedPreference();
    }

    //citirea din fisier de preferinta
    private void loadFromSharedPreference(){
        String userSP=preferences.getString(USER,"");
        String passwordSP=preferences.getString(PASSWORD,"");
        //incarcare valori din fisier in comp vizuale
        user.setText(userSP);
        passwd.setText(passwordSP);
    }


}