package com.example.proiectandroidbun;

import com.example.proiectandroidbun.database.model.Task;

import java.util.ArrayList;

public interface ITask {
    void onSuccess(ArrayList<Task> tasks);
    void onFailure(Throwable error);

}
