package com.example.proiectandroidbun;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.proiectandroidbun.database.model.Task;

import java.util.ArrayList;

public class TODOadapter  extends BaseAdapter {

    private ArrayList<Task> tasks;
    private Context context;
    private LayoutInflater inflater;

    public TODOadapter(ArrayList<Task> tasks, Context context) {
        this.tasks = tasks;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public Object getItem(int position) {
        return tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final View taskView=inflater.inflate(R.layout.item_todo, viewGroup, false);
        TextView tvTitle=taskView.findViewById(R.id.tv_taskTitle);
        TextView tvDescription=taskView.findViewById(R.id.tv_taskDescription);
        TextView tvDeadline=taskView.findViewById(R.id.tv_taskDeadline);
        TextView tvCategory=taskView.findViewById(R.id.tv_category);
        TextView tvTimeforStart=taskView.findViewById(R.id.tv_timeforstart);
        Task task=tasks.get(position);
        tvTitle.setText(task.getTitle());
        tvDescription.setText(task.getDescription());
        tvDeadline.setText(task.getTheDate());
        tvCategory.setText(task.getCategory());
        tvTimeforStart.setText(task.getStartTime());
        return taskView;

    }

    public void addElements(ArrayList<Task> list)
    {
        tasks.addAll(list);
        notifyDataSetChanged();
    }

    public void  removeElement(int position)
    {
        tasks.remove(position);
        notifyDataSetChanged();
    }
}
