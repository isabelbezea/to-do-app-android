package com.example.proiectandroidbun.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.proiectandroidbun.database.model.Account;
import com.example.proiectandroidbun.database.model.Task;

import java.util.List;
@Dao
public interface AccountDao {

    @Query("select * from accounts")
    List<Account> getAll();

    @Query("select * from accounts where username=(:username) and password=(:password)")
    Account login(String username, String password);

    @Insert
    long insert(Account account);//long represents the value of id

    @Update
    int update(Account account);//int represents the number of affected rows

    @Delete
    int delete(Account account);}


