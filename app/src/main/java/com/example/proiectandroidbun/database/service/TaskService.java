package com.example.proiectandroidbun.database.service;

import android.content.Context;


import com.example.proiectandroidbun.asyncTask.AsyncTaskRunner;
import com.example.proiectandroidbun.asyncTask.Callback;
import com.example.proiectandroidbun.database.DatabaseManager;
import com.example.proiectandroidbun.database.dao.TaskDao;
import com.example.proiectandroidbun.database.model.Task;

import java.util.List;
import java.util.concurrent.Callable;

public class TaskService {

    private final TaskDao taskDao;
    private final AsyncTaskRunner taskRunner;

    public TaskService(Context context){
        taskDao = DatabaseManager.getInstance(context).getTaskDao();
        taskRunner=new AsyncTaskRunner();

    }

    public List<Task> getAllV2() {
        return taskDao.getAll();
    }

    public void getAll(Callback<List<Task>> callback){
        Callable<List<Task>> callable=new Callable<List<Task>>(){

            @Override
            public List<Task> call() throws Exception {
                return taskDao.getAll();
            }
        };
        taskRunner.executeAsync(callable, callback);
    }


    public void getBILLS(Callback<List<Task>> callback){
        Callable<List<Task>> callable=new Callable<List<Task>>(){

            @Override
            public List<Task> call() throws Exception {
                return taskDao.getBILLS();
            }
        };
        taskRunner.executeAsync(callable, callback);
    }

    public void insert(Callback<Task> callback, final Task task)
    {
        Callable<Task>callable=new Callable<Task>() {
            @Override
            public Task call() {
               if(task==null) return null;
               long id=taskDao.insert(task);
               if(id==-1) return null;
               task.setId(id);
               return task;

            }
        };
        taskRunner.executeAsync(callable, callback);
    }

    public void update(Callback<Task> callback, final Task task) {
        Callable<Task> callable = new Callable<Task>() {
            @Override
            public Task call() {
                if (task == null) {
                    return null;
                }
                int count = taskDao.update(task);
                if (count < 1) {
                    return null;
                }
                return task;
            }
        };
        taskRunner.executeAsync(callable, callback);
    }

    public void delete(Callback<Integer> callback, final Task task) {
        Callable<Integer> callable = new Callable<Integer>() {
            @Override
            public Integer call() {
                if (task == null) {
                    return -1;
                }
                return taskDao.delete(task);
            }
        };
        taskRunner.executeAsync(callable, callback);
    }

}
