package com.example.proiectandroidbun.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.proiectandroidbun.database.model.Task;

import java.util.List;

@Dao
public interface TaskDao {
    @Query("select * from tasks where category!='BILLS'")
    List<Task> getAll();


    @Query("select * from tasks where category=='BILLS'")
    List<Task> getBILLS();


    @Insert
    long insert(Task task);//long represents the value of id

    @Update
    int update(Task task);//int represents the number of affected rows

    @Delete
    int delete(Task task);}


