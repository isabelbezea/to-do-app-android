package com.example.proiectandroidbun.database.dao;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.proiectandroidbun.database.model.UserAndTasks;

import java.util.List;

@Dao
public interface UserAndTasksDao {
    @Transaction
    @Query("SELECT * FROM accounts")
    public List<UserAndTasks> getUserWithTasks();
}
