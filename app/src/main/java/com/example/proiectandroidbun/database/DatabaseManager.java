package com.example.proiectandroidbun.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.proiectandroidbun.database.dao.AccountDao;
import com.example.proiectandroidbun.database.dao.TaskDao;
import com.example.proiectandroidbun.database.model.Account;
import com.example.proiectandroidbun.database.model.Task;
import com.example.proiectandroidbun.database.service.TaskService;

@Database(entities={Task.class, Account.class}, exportSchema=false, version = 4)
public abstract class DatabaseManager extends RoomDatabase {

    private static final String PROIECT_DB="proiect_db";
    private static DatabaseManager databaseManager;

    public static DatabaseManager getInstance(Context context){
        if(databaseManager==null) {
        synchronized (DatabaseManager.class) {
            if (databaseManager == null) {
                databaseManager = Room.databaseBuilder(context, DatabaseManager.class, PROIECT_DB).fallbackToDestructiveMigration().build();
            }

        }
}

        return databaseManager;

    }
    //ne asiguram ca primim o leg la baza de date
    //e obligatoriu, orice initializare de dao se face prin manager, el este cel  care proiecteaza bd si cel care obtine legatura
    public  abstract TaskDao getTaskDao();

    public abstract AccountDao getAccountDao();

}
