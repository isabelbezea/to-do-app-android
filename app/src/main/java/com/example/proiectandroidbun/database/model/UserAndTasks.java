package com.example.proiectandroidbun.database.model;

import androidx.room.Relation;

import java.util.List;

public class UserAndTasks {

    public Account account;
    @Relation(
            parentColumn = "username",
            entityColumn = "id"
    )
    public List<Task> tasks;

}
