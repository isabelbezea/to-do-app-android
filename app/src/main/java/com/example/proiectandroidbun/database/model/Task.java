package com.example.proiectandroidbun.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "tasks")
public class Task  implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    private long id;

    @ColumnInfo(name="taskTitle")
    private String title;
    @ColumnInfo(name="taskDescription")
    private  String description;
    @ColumnInfo(name="startTime")
    private String startTime;
    @ColumnInfo(name="endTime")
    private String endTime;
    @ColumnInfo(name="category")
    private String category;
    @ColumnInfo(name="date")
    private String theDate;



    public Task(long id, String title, String description, String startTime, String endTime, String category, String theDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.category = category;
        this.theDate = theDate;

    }


    @Ignore
    public Task (){ }
    @Ignore
    public Task(String title, String description, String startTime, String endTime, String category, String theDate) {
        this.title = title;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.category = category;
        this.theDate=theDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTheDate() {
        return theDate;
    }

    public void setTheDate(String theDate) {
        this.theDate = theDate;
    }



    @Override
    public String toString() {
        return "Titlu task: "+title+"\n Descriere: "+description+"\n Data: "+theDate+"\n Interval: "+startTime+"-"+endTime+"\n Categorie: "+category;
    }
}
