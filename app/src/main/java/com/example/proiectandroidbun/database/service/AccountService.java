package com.example.proiectandroidbun.database.service;

import android.content.Context;

import com.example.proiectandroidbun.asyncTask.AsyncTaskRunner;
import com.example.proiectandroidbun.asyncTask.Callback;
import com.example.proiectandroidbun.database.DatabaseManager;
import com.example.proiectandroidbun.database.dao.AccountDao;
import com.example.proiectandroidbun.database.model.Account;
import com.example.proiectandroidbun.database.model.Task;

import java.util.List;
import java.util.concurrent.Callable;


public class AccountService {

    private final AccountDao accountDao;
    private final AsyncTaskRunner taskRunner;

    public AccountService(Context context){
        accountDao = DatabaseManager.getInstance(context).getAccountDao();
        taskRunner=new AsyncTaskRunner();

    }

    public List<Account> getAllV2() {
        return accountDao.getAll();
    }

    public void getAll(Callback<List<Account>> callback){
        Callable<List<Account>> callable=new Callable<List<Account>>(){

            @Override
            public List<Account> call() throws Exception {
                return accountDao.getAll();
            }
        };
        taskRunner.executeAsync(callable, callback);
    }



    public void insert(Callback<Account> callback, final Account account)
    {
        Callable<Account>callable=new Callable<Account>() {
            @Override
            public Account call() {
                if(account==null) return null;
                long id=accountDao.insert(account);
                if(id==-1) return null;
                account.setId(id);
                return account;

            }
        };
        taskRunner.executeAsync(callable, callback);
    }
    public void update(Callback<Account> callback, final Account account) {
        Callable<Account> callable = new Callable<Account>() {
            @Override
            public Account call() {
                if (account == null) {
                    return null;
                }
                int count = accountDao.update(account);
                if (count < 1) {
                    return null;
                }
                return account;
            }
        };
        taskRunner.executeAsync(callable, callback);
    }

    public void delete(Callback<Integer> callback, final Account account) {
        Callable<Integer> callable = new Callable<Integer>() {
            @Override
            public Integer call() {
                if (account == null) {
                    return -1;
                }
                return accountDao.delete(account);
            }
        };
        taskRunner.executeAsync(callable, callback);
    }




}
