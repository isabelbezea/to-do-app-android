package com.example.proiectandroidbun.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName = "accounts")
public class Account implements Serializable {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    private long id;

    @ColumnInfo(name="username")
    private String username;
    @ColumnInfo(name="password")
    private String password;
    @ColumnInfo(name="address")
    private String address;
    @ColumnInfo(name="birthday")
    private String birthday;
    @ColumnInfo(name="country")
    private String country;

    public Account(){}

    public Account(long id, String username, String password, String address, String birthday, String country) {
       this.id=id;
        this.username = username;
        this.password = password;
        this.address = address;
        this.birthday = birthday;
        this.country=country;
    }

    /*public Account(String username, String password) {
        this.username = username;
        this.password = password;
    }*/


    @Ignore
    public Account(String username, String password, String address, String birthday, String country) {
        this.username = username;
        this.password = password;
        this.address = address;
        this.birthday = birthday;
        this.country = country;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    @Override
    public String toString() {
        return "Account{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", address='" + address + '\'' +
                ", birthday='" + birthday + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
