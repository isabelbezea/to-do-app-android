package com.example.proiectandroidbun;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CustomAdapterSpinner extends ArrayAdapter{


    public CustomAdapterSpinner(Context context, ArrayList<Flag> flagList ){
        super(context, 0, flagList);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.group_item, parent, false);

        }
        ImageView imageViewFlag = convertView.findViewById(R.id.iv_romania);
        TextView textViewFlag = convertView.findViewById(R.id.tv_romania);

        Flag currentItem = (Flag) getItem(position);
        if (currentItem != null) {
            imageViewFlag.setImageResource(currentItem.getFlagImage());
            textViewFlag.setText(currentItem.getFlag());
        }

            return convertView;
        }
    }


