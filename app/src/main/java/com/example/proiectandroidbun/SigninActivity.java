package com.example.proiectandroidbun;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.proiectandroidbun.asyncTask.Callback;
import com.example.proiectandroidbun.database.model.Account;
import com.example.proiectandroidbun.database.model.Task;
import com.example.proiectandroidbun.database.service.AccountService;
import com.example.proiectandroidbun.database.service.TaskService;

import java.util.ArrayList;
import java.util.Calendar;

//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;

public class SigninActivity extends AppCompatActivity {

    private TextView tvDate;
    private DatePickerDialog.OnDateSetListener dpdDate;

    private EditText etUsername;
    private EditText etPassword;
    private EditText etMail;
    private Button btnRegister;

    public static final String ACCOUNT_KEY = "accountKey";
    private AccountService accountService;
    public static ArrayList<Account> accounts_list = new ArrayList<>();
    private ArrayList<Flag> flagList;
    private CustomAdapterSpinner customAdapterSpinner;
    public String clickedFlagName;
    private Intent intent;
    private Account account=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_activity);


        tvDate = (TextView) findViewById(R.id.tv_dateS);

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(SigninActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, dpdDate, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });


        dpdDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                Log.v("SigninActivity", "onDateSet: dd/mm/yyyy: " + dayOfMonth + "/" + month + "/" + year);


                String date = dayOfMonth + "/" + month + "/" + year;
                tvDate.setText(date);
            }
        };


        initViews();
        setupListeners();

        initList();

        Spinner spinnerForStages = findViewById(R.id.sp_spinnerS);
        customAdapterSpinner = new CustomAdapterSpinner(this, flagList);
        spinnerForStages.setAdapter(customAdapterSpinner);


        spinnerForStages.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Flag clickedItem = (Flag) parent.getItemAtPosition(position);
                 clickedFlagName = clickedItem.getFlag();
                Toast.makeText(SigninActivity.this, clickedFlagName + " selected", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void initViews() {
        etUsername = findViewById(R.id.et_usernameS);
        etPassword = findViewById(R.id.et_passwordS);
        etMail = findViewById(R.id.et_mailS);
        tvDate = findViewById(R.id.tv_dateS);
        btnRegister = findViewById(R.id.button_register);
    }

    private boolean validateContent() {
        if (etUsername.getText().toString().isEmpty()) {
            Toast.makeText(this, "Username-ul trebuie introdus", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "Parola trebuie introdusa", Toast.LENGTH_LONG).show();
            return false;
        }

        if (etMail.getText().toString().isEmpty()) {
            Toast.makeText(this, "Adresa de mail trebuie introdusa", Toast.LENGTH_LONG).show();
            return false;
        }

        //verificare daca ce am introdus in email are forma specifica
        if (!Patterns.EMAIL_ADDRESS.matcher(etMail.getText().toString()).matches()) {
            Toast.makeText(this, "Adresa de mail format invalid", Toast.LENGTH_LONG).show();
            return false;
        }

        if (tvDate.getText().toString().isEmpty()) {
            Toast.makeText(this, "Data de nastere trebuie introdusa", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;

    }

    //incarcam butonul cu o actiune
    private void setupListeners() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateContent()) {

                    Account account = new Account();
                    account.setUsername(etUsername.getText().toString());
                    account.setPassword(etPassword.getText().toString());
                    account.setAddress(etMail.getText().toString());
                    account.setBirthday(tvDate.getText().toString());
                    account.setCountry(clickedFlagName);

                    accountService = new AccountService(getApplicationContext());
                    accountService.insert(insertIntoDbCallback(), account);
                    Toast.makeText(SigninActivity.this, "User registered!", Toast.LENGTH_SHORT).show();
                    finish();

                }
            }
        });

    }

    private void initList() {
        flagList = new ArrayList<>();
        flagList.add(new Flag("Romania", R.drawable.romania));
        flagList.add(new Flag("Norway", R.drawable.norway));
        flagList.add(new Flag("France", R.drawable.france));
        flagList.add(new Flag("Spain", R.drawable.spain));
        flagList.add(new Flag("Greece", R.drawable.greece));


    }

    private Callback<Account> insertIntoDbCallback() {
        return new Callback<Account>() {
            @Override
            public void runResultOnUiThread(Account result) {
                if (result != null) {
                    accounts_list.add(result);
                    //notifyAdapter();
                }
            }
        };
    }


}