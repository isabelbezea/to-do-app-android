package com.example.proiectandroidbun;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.proiectandroidbun.database.model.Task;

import java.util.ArrayList;

public class RatingAdapter extends BaseAdapter {

    private ArrayList<Rating> ratings;
    private Context context;
    private LayoutInflater inflater;

    public RatingAdapter(ArrayList<Rating> ratings, Context context) {
        this.ratings = ratings;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return ratings.size();
    }

    @Override
    public Object getItem(int position) {
        return ratings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final View ratingView=inflater.inflate(R.layout.item_rating, viewGroup, false);
        RatingBar ratingBar=ratingView.findViewById(R.id.ratingBarI);
        TextView tvRating=ratingView.findViewById(R.id.tv_ratingBar);
        TextView tvDescription=ratingView.findViewById(R.id.tv_descriereI2);
        Rating rating=ratings.get(position);
        ratingBar.setRating(rating.getRating());
        tvRating.setText(String.valueOf(rating.getRating())+"/5");
        tvDescription.setText(rating.getAdvice());
        return ratingView;
    }


    public void addElements(ArrayList<Rating> list)
    {
        ratings.addAll(list);
        notifyDataSetChanged();
    }

    public void  removeElement(int position)
    {
        ratings.remove(position);
        notifyDataSetChanged();
    }

}
