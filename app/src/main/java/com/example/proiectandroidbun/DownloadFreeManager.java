package com.example.proiectandroidbun;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class DownloadFreeManager {

    private static DownloadFreeManager instance;

    private DownloadFreeManager(){}

    public static DownloadFreeManager getInstance(){
        if(instance==null) instance=new DownloadFreeManager();
        return instance;
    }

    public void getFreeDaysData(final IFree listener){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("https://api.mocki.io/v1/0dfa8065");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    InputStream stream = connection.getInputStream();
                    InputStreamReader reader = new InputStreamReader(stream);
                    BufferedReader bufferedReader = new BufferedReader(reader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String line = "";

                    while ((line = bufferedReader.readLine()) != null)
                        stringBuilder.append(line);

                    bufferedReader.close();
                    reader.close();
                    stream.close();

                    listener.onSuccess(parseDayData(stringBuilder.toString()));

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    listener.onFailure(e);
                } catch (IOException e) {
                    e.printStackTrace();
                    listener.onFailure(e);
                }
            }
        }).start();
    }

    private ArrayList<FreeDay> parseDayData(String result){
        ArrayList<FreeDay> freeDays=new ArrayList<>();
        try{
            JSONObject resultJson=new JSONObject(result);

            String an1=resultJson.getString("an");
            String tip1=resultJson.getString("tip");

            JSONArray daysArray=resultJson.getJSONArray("liber");
            Log.v("days", daysArray.toString());
            for(int i=0;i<daysArray.length();i++)
            {
                JSONObject currentObj=daysArray.getJSONObject(i);
                String name1=currentObj.getString("denumire");
                String data1=currentObj.getString("data");
                String durata1=currentObj.getString("durata");

                FreeDay freeDay=new FreeDay(name1,data1,durata1, an1, tip1);
                freeDays.add(freeDay);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return freeDays;
    }


}
