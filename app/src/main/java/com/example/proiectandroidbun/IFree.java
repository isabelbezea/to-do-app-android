package com.example.proiectandroidbun;

import java.util.ArrayList;

public interface IFree {
    void onSuccess(ArrayList<FreeDay> result);
    void onFailure(Throwable error);
}
