package com.example.proiectandroidbun;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.proiectandroidbun.database.model.Task;

import java.util.ArrayList;

public class BillAdaptor extends BaseAdapter
{

    private ArrayList<Task> bills;
    private Context context;
    private LayoutInflater inflater;

    public BillAdaptor(ArrayList<Task> bills, Context context) {
        this.bills = bills;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return bills.size();
    }

    @Override
    public Object getItem(int position) {
        return bills.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
         final View billView=inflater.inflate(R.layout.bill_item, viewGroup, false);
        TextView tvFurnizor=billView.findViewById(R.id.tv_furnizorB);
        TextView tvData=billView.findViewById(R.id.tv_taskDeadlineB);
        TextView tvCategory=billView.findViewById(R.id.tv_categoryB);
        Task task=bills.get(position);
        tvFurnizor.setText("FURNIZOR: "+task.getDescription());
        tvData.setText(task.getTheDate());
        tvCategory.setText(task.getCategory());
        return billView;

    }

    public void addElements(ArrayList<Task> list)
    {
        bills.addAll(list);
        notifyDataSetChanged();
    }

    public void  removeElement(int position)
    {
        bills.remove(position);
        notifyDataSetChanged();
    }

}
