package com.example.proiectandroidbun;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.proiectandroidbun.asyncTask.Callback;
import com.example.proiectandroidbun.database.model.Task;
import com.example.proiectandroidbun.database.service.TaskService;

import java.util.ArrayList;
import java.util.List;

public class ToDoTasks extends AppCompatActivity {

    public static int ADD_REQUEST_CODE = 201;
    private static final int ADD_EXPENSE_REQUEST_CODE = 201;
    private static final int UPDATE_EXPENSE_REQUEST_CODE = 202;
    private ListView lvTasks;
    public TODOadapter taskTODOadapter;
    public static ArrayList<Task> task_list = new ArrayList<>();
    private TaskService taskService;

    private ArrayList<Task> add_a_task_to_a_list(ArrayList<Task> new_task_list, Task task) {

        new_task_list.add(task);
        return new_task_list;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_tasks);
        lvTasks = findViewById(R.id.lv_tasksTODO);

        addAdapter();
        lvTasks.setOnItemClickListener(updateEventListener());
        lvTasks.setOnItemLongClickListener(deleteEventListener());

        taskService = new TaskService(getApplicationContext());//contextul e folosit pt deschiderea bazei de date
        taskService.getAll(getAllFromDbCallBack());

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            Task task = (Task) data.getSerializableExtra(TaskActivity.TASK_KEY);
            Log.v("pentru noi", task.toString());

            if (requestCode == UPDATE_EXPENSE_REQUEST_CODE) {
                taskService.update(updateToDbCallback(), task);
            }
        }
        else
            Log.v("eroareeee","n-am intrat  in if");
    }

    private void getAllFromDbV2() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                final List<Task> results = taskService.getAllV2();
                new Handler(getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (results != null) {
                            task_list.clear();
                            task_list.addAll(results);
                            notifyAdapter();
                        }
                    }
                });
            }
        };
        thread.start();
    }

    private void notifyAdapter() {
        TODOadapter adapter = (TODOadapter) lvTasks.getAdapter();
        adapter.notifyDataSetChanged();
    }

    //zona unde vom primi rezultatul executat prin service
    private Callback<List<Task>> getAllFromDbCallBack() {
        return new Callback<List<Task>>() {
            @Override
            public void runResultOnUiThread(List<Task> result) {
                if (result != null ) {
                    task_list.clear();
                    task_list.addAll(result);
                    notifyAdapter();

                }
            }
        };
    }


    private Callback<Task> updateToDbCallback() {
        return new Callback<Task>() {
            @Override
            public void runResultOnUiThread(Task result) {
                if (result != null) {
                    for (Task task : task_list) {
                        if (task.getId() == result.getId()) {
                            task.setTitle(result.getTitle());
                            task.setDescription(result.getDescription());
                            task.setCategory(result.getCategory());
                            task.setStartTime(result.getStartTime());
                            task.setEndTime(result.getEndTime());
                            task.setTheDate(result.getTheDate());
                            break;
                        }
                    }
                    notifyAdapter();
                }
            }
        };
    }

    private Callback<Integer> deleteToDbCallback(final int position) {
        return new Callback<Integer>() {
            @Override
            public void runResultOnUiThread(Integer result) {
                if (result != -1) {
                    task_list.remove(position);
                    notifyAdapter();
                }
            }
        };
    }

    private AdapterView.OnItemClickListener updateEventListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), TaskActivity.class);
                intent.putExtra(TaskActivity.TASK_KEY, task_list.get(position));
                startActivityForResult(intent, UPDATE_EXPENSE_REQUEST_CODE);
            }
        };
    }

    private AdapterView.OnItemLongClickListener deleteEventListener() {
        return new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                taskService.delete(deleteToDbCallback(position), task_list.get(position));
                return true;
            }
        };
    }


    private void addAdapter() {
        taskTODOadapter = new TODOadapter(task_list, this);
        lvTasks.setAdapter(taskTODOadapter);
    }

}

