# TO-DO App Android

**The purpose of the application**

It is well known that personal goals and good organization are essential for each of us. In this regard, my purpose was to develop an application that will use various tools in order to manage time and achieve goals in an efficient way.
"My agenda" is a simple but effective application for those who want to keep track of future activities for better organization.
I tried to create an interface that is as user-friendly as possible and easy to use, so that the introduction of activities can be carried out in a simple and fast way.

**Application structure**

In this sense, after the user goes through the authentication / log in stage of the application, the home page  will be opened, containing various controls to the different activities of the application, arranged in a  compact way.
Also, through a menu, the user can give feedback / can view the opinions of other users.


**Way of ussage**

_**Log in / Sign in**_

The first step that the user has to do to access the application is to log in.
If the account does not exist in the **database**, a corresponding message will be displayed informing the user that he must create an account.


![](images/login.PNG)

Through visual controls, the user can enter their date of birth in a simple and fast way, and has the possibility to choose the country of origin from a predefined list.

![](images/createAccount.PNG)

_**Main screen with derived activities**_

After the user has successfully logged in, he will be greeted with a message containing his username, due to **preference files**.
Within this page, to enter a task, the user must access the "CALENDAR" button that will redirect him to a real-time calendar, from where he can select the day he wants to schedule an activity for.
After selecting the day, a form for adding tasks will be opened automatically. The “START TIME” and “END TIME” buttons will open a time picker to facilitate data entry. By pressing the "SAVE" button, the task will be automatically **registered in the database**, and by "CANCEL" the user will return to the previous activity.

![](images/calendar.PNG)

Through the task addition activity, the user can also add incoming bills, and by checking the BILLS category, they will be automatically redirected to another list. Each list will be accessed through the "SHOW MY TASKS" and "SHOW MY BILLS" buttons.

![](images/json.PNG)

For a better planning of activities, I decided to inform the user about those Christian holidays during 2021 that are declared days off from the State, providing an activity where they are listed. This was created by importing a **JSON file**.

_**Menu**_

By accessing the menu, the user can give feedback based on the experience of using the application, but also to see the reviews of other user. By accessing this menu, the user can also be disconnected from the application and is automatically redirected to the log in page. Each feedback is stored in **Firebase**.

![](images/feedback.PNG)

Thank you for your attention!
